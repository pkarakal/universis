import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import * as STUDYPROGRAMS_LIST_CONFIG from './study-programs-table.config.active.json';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component.js';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-study-programs-table',
  templateUrl: './study-programs-table.component.html',
  styles: []
})
export class StudyProgramsTableComponent implements OnInit, OnDestroy  {

  public readonly config = STUDYPROGRAMS_LIST_CONFIG;
  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  public recordsTotal: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    console.log(data);
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
