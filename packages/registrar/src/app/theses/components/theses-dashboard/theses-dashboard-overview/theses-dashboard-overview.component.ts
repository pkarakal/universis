import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-overview',
  templateUrl: './theses-dashboard-overview.component.html'
})
export class ThesesDashboardOverviewComponent implements OnInit {
  public model: any;


  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Theses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person))')
      .getItem();
  }
}
