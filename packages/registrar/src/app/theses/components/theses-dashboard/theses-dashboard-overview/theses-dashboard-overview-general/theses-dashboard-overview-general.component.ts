import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-theses-dashboard-overview-general',
  templateUrl: './theses-dashboard-overview-general.component.html',
})
export class ThesesDashboardOverviewGeneralComponent implements OnInit {
  public thesis: any;
  public thesisId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this.thesisId = this._activatedRoute.snapshot.params.id;

    this.thesis = await this._context.model('Theses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person))')
      .getItem();
  }

}
