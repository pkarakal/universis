import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../../../tables/components/advanced-table/advanced-table.component';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {ActivatedTableService} from '../../../../../tables/tables.activated-table.service';


@Component({
  selector: 'app-theses-dashboard-overview-members',
  templateUrl: './theses-dashboard-overview-members.component.html',
})
export class ThesesDashboardOverviewMembersComponent implements OnInit {
  public model: any;

  @ViewChild('members') members: AdvancedTableComponent;
  thesesID: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this._activatedTable.activeTable = this.members;


    this.model = await this._context.model('ThesisRoles')
      .where('thesis').equal(this._activatedRoute.snapshot.params.id)
      .expand('member($expand=department)')
      .getItems();
  }

}
