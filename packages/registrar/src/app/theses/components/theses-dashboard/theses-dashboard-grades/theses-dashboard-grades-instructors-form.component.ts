import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-grades-instructors-form',
  templateUrl: './theses-dashboard-grades-instructors-form.component.html'
})
export class ThesesDashboardGradesInstructorsFormComponent implements OnInit {

//  @Input() studentId: any;
  @Input() grades: any;
  @Input() editMode = false;
  @Input() grateScale: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
  }

}
