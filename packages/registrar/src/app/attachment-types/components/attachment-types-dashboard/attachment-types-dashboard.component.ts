import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-attachment-types-dashboard',
  templateUrl: './attachment-types-dashboard.component.html',
  styleUrls: ['./attachment-types-dashboard.component.scss']
})
export class AttachmentTypesDashboardComponent implements OnInit {
  public model: any;
  public tabs: any[];

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    this.model = await this._context.model('AttachmentTypes')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
  }
}
