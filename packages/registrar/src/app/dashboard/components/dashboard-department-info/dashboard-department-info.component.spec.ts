import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardDepartmentInfoComponent } from './dashboard-department-info.component';

describe('DashboardDepartmentInfoComponent', () => {
  let component: DashboardDepartmentInfoComponent;
  let fixture: ComponentFixture<DashboardDepartmentInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardDepartmentInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardDepartmentInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
