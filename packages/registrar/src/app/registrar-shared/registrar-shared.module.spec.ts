import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RegistrarSharedModule} from './registrar-shared.module';
import {AppSidebarService} from '@universis/common';
describe('InstructorsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      providers: [
          AppSidebarService
      ]
    }).compileComponents();
  }));
  it('should create an instance', inject([TranslateService], (_translateService: TranslateService,
                                                              _appSidebarService: AppSidebarService) => {
    const registrarSharedModule = new RegistrarSharedModule(null, _translateService, _appSidebarService);
    expect(registrarSharedModule).toBeTruthy();
  }));
});
