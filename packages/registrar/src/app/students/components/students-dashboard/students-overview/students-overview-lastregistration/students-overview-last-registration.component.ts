import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-lastregistration',
  templateUrl: './students-overview-last-registration.component.html',
  styleUrls: ['./students-overview-last-registration.component.scss']
})
export class StudentsOverviewLastRegistrationComponent implements OnInit, OnDestroy  {
  public lastRegistration: any;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    try {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        this.lastRegistration = await this._context.model('Students/' + this.studentId + '/LastPeriodRegistration')
          .asQueryable()
          // tslint:disable-next-line:max-line-length
          .expand('documents($orderby=dateCreated desc;$top=1),classes($select=registration,sum(ects) as ects,count(id) as total;$groupby=registration)')
          .getItem();
      });
    } catch (error) {

    }

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
