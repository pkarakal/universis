import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewProfileComponent } from './students-overview-profile.component';

describe('StudentsOverviewProfileComponent', () => {
  let component: StudentsOverviewProfileComponent;
  let fixture: ComponentFixture<StudentsOverviewProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
