import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-registrations',
  templateUrl: './students-registrations.component.html'
})
export class StudentsRegistrationsComponent implements OnInit, OnDestroy  {
  public model: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Students/' + params.id + '/registrations')
        .asQueryable()
        .expand('classes($expand=course,courseType,courseClass($expand=course($expand=instructor)),examPeriod)')
        .orderByDescending('registrationYear')
        .thenByDescending('registrationPeriod')
        .getItems();
      this._loadingService.hideLoading();
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
