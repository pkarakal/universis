import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableDataResult
} from '../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-departments-table',
  templateUrl: './departments-table.component.html'
})
export class DepartmentsTableComponent implements  OnInit , OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  public recordsTotal: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;


  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTableService: ActivatedTableService) { }


  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTableService.activeTable = this.table;
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.table.query = null;
        this.table.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
