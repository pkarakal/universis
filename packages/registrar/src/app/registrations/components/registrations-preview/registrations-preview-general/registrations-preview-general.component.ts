import { Component, OnInit, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registrations-preview-general',
  templateUrl: './registrations-preview-general.component.html'
})
export class RegistrationsPreviewGeneralComponent implements OnInit {

  @Input() classes: any;
  @Input() totals: any;
  
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

    this.classes = await this._context.model('StudentPeriodRegistrations/' + this._activatedRoute.snapshot.params.id + '/classes')
    .asQueryable()
    .expand('semester,course($expand=instructor,courseSector,department),courseClass($expand=instructors($expand=instructor))')
    .take(-1)
    .getItems();

    this.totals = await this._context.model('StudentPeriodRegistrations/' + this._activatedRoute.snapshot.params.id + '/classes')
    .asQueryable()
    .select('sum(units) as sumUnits,sum(ects) as sumECTS')
    .getItem();

  }

}
