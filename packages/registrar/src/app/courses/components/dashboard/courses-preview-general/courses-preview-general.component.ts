import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-preview-general',
  templateUrl: './courses-preview-general.component.html'
})
export class CoursesPreviewGeneralComponent implements OnInit, OnDestroy {

  @Input() model: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Courses')
        .where('id').equal(params.id)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
