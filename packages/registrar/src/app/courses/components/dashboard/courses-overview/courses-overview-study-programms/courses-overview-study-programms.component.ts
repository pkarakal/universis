import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-study-programms',
  templateUrl: './courses-overview-study-programms.component.html',
  styleUrls: ['./courses-overview-study-programms.component.scss']
})
export class CoursesOverviewStudyProgrammsComponent implements OnInit, OnDestroy {
  public courseId: any;
  public model: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('ProgramCourses')
        .where('course').equal(params.id)
        .expand('course,studyProgramSpecialty,program($expand=department,studyLevel)')
        .getItems()
      this.courseId = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
