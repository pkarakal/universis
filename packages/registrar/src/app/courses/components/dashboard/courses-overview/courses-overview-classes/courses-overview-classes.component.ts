import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview-classes',
  templateUrl: './courses-overview-classes.component.html',
  styleUrls: ['./courses-overview-classes.component.scss']
})
export class CoursesOverviewClassesComponent implements OnInit, OnDestroy {
  @Input() currentYear: any;
  public courseId: any;
  public model: any;
  public currentCourseClasses: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
}


  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.currentCourseClasses = await this._context.model('CourseClasses')
        .where('course').equal(this._activatedRoute.snapshot.params.id)
        .expand('period,status,instructors($expand=instructor),course($expand=department),students($select=courseClass,count(id) as total;$groupby=courseClass)')
        .and('year').equal({'$name': '$it/course/department/currentYear'})
        .getItems();

      this.courseId = params.id;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
