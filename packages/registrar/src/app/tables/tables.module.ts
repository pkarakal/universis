import {InjectionToken, NgModule, OnInit, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedTableComponent, COLUMN_FORMATTERS} from './components/advanced-table/advanced-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {AdvancedTableSearchComponent} from './components/advanced-table/advanced-table-search.component';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {BtnExpDirective} from './directives/btnexp.directive';
import {AdvancedTableSettingsComponent} from './components/advanced-table/advanced-table-settings.component';
import {SharedModule, TemplatePipe} from '@universis/common';
import {AdvancedFilterValueProvider} from './components/advanced-table/advanced-filter-value-provider.service';
import { AdvancedTableModalBaseComponent } from './components/advanced-table-modal/advanced-table-modal-base.component';
import {RouterModalModule} from '@universis/common/routing';
import { AdvancedSearchFormComponent } from './components/advanced-search-form/advanced-search-form.component';
import {AdvancedFormsModule} from '@universis/forms';
import {FormioModule} from 'angular-formio';
import { AdvancedListComponent } from './components/advanced-list/advanced-list.component';
import {RouterModule} from '@angular/router';
import {
    AdvancedSearchConfigurationResolver,
    AdvancedTableConfigurationResolver
} from './components/advanced-table/advanced-table-resolvers';
import {AdvancedRowActionComponent} from './components/advanced-row-action/advanced-row-action.component';
import {FORMATTERS} from './components/advanced-table/advanced-table.formatters.interface';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        RouterModalModule,
        FormioModule,
        AdvancedFormsModule
    ],
    providers: [DatePipe, TemplatePipe,
      {
        provide: COLUMN_FORMATTERS,
        useValue: FORMATTERS
      },

      AdvancedTableConfigurationResolver,
        AdvancedSearchConfigurationResolver
      ],
    declarations: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedTableModalBaseComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent
    ],
    exports: [
        AdvancedTableComponent,
        AdvancedTableSearchComponent,
        BtnExpDirective,
        AdvancedTableSettingsComponent,
        AdvancedSearchFormComponent,
        AdvancedListComponent,
        AdvancedRowActionComponent
    ],
    entryComponents: [
        AdvancedTableSettingsComponent,
        AdvancedRowActionComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TablesModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.error('An error occurred while loading tables module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach(language => {
            import(`./i18n/tables.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }

}
