import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGradesCheckComponent } from './exams-preview-grades-check.component';

describe('ExamsPreviewGradesCheckComponent', () => {
  let component: ExamsPreviewGradesCheckComponent;
  let fixture: ComponentFixture<ExamsPreviewGradesCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGradesCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGradesCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
