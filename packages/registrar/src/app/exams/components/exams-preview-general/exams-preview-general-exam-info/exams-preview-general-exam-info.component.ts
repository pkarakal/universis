import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-general-exam-info',
  templateUrl: './exams-preview-general-exam-info.component.html',
  styleUrls: ['./exams-preview-general-exam-info.component.scss']
})
export class ExamsPreviewGeneralExamInfoComponent implements OnInit, OnDestroy  {

  public courseExam: any;
  public uploadGrades: any;
  public examId: any;
  private subscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.examId = params.id;

      this.courseExam = await this._context.model('CourseExams')
        .where('id').equal(params.id)
        .expand('course,examPeriod,status,completedByUser,year')
        .getItem();
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
