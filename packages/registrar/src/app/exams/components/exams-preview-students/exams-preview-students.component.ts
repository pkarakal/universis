import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as EXAMS_STUDENTS_LIST_CONFIG from './exams-students-table.config.list.json';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-exams-preview-students',
  templateUrl: './exams-preview-students.component.html',
  styleUrls: ['./exams-preview-students.component.scss']
})
export class ExamsPreviewStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> EXAMS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public examId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  // @Input() tableConfiguration: any;
  // @Input() searchConfiguration: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _context: AngularDataContext
  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.examId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('CourseExams/' + this.examId + '/students')
        .asQueryable()
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(EXAMS_STUDENTS_LIST_CONFIG);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'CourseExams/' + params.id + '/students';

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
