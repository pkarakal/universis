import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { StudentsSharedModule } from '../students/students.shared';
import * as DEFAULT_GRADUATIONS_LIST from './components/graduations-table/default-graduations-table.config.json';
import {
  GraduationsDefaultTableConfigurationResolver,
  GraduationsTableSearchResolver,
  GraduationsTableConfigurationResolver
} from './components/graduations-table/graduations-table-config.resolver';
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        StudentsSharedModule
    ],
    providers: [
        GraduationsDefaultTableConfigurationResolver,
        GraduationsTableSearchResolver,
        GraduationsTableConfigurationResolver
    ]
})
export class GraduationsSharedModule implements OnInit {
  public static readonly DefaultGraduationsList = DEFAULT_GRADUATIONS_LIST;
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch((err) => {
            console.error('An error occurred while loading graduations shared module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach((language) => {
            import(`./i18n/graduations.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }
}
