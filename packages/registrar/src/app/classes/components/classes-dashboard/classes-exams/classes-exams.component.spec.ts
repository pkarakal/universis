import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesExamsComponent } from './classes-exams.component';

describe('ClassesExamsComponent', () => {
  let component: ClassesExamsComponent;
  let fixture: ComponentFixture<ClassesExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
