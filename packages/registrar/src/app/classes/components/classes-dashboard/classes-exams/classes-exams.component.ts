import {Component, OnInit, ViewChild, Input, EventEmitter, OnDestroy} from '@angular/core';
import { ActivatedTableService } from 'packages/registrar/src/app/tables/tables.activated-table.service';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-exams',
  templateUrl: './classes-exams.component.html',
  styleUrls: ['./classes-exams.component.scss']
})
export class ClassesExamsComponent implements OnInit, OnDestroy {

  public classExams: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classExams = await this._context.model('CourseExamClasses')
        .where('courseClass').equal(params.id)
        .expand('courseExam($expand=examPeriod,status,completedByUser,year,course($expand=department))')
        .orderByDescending('courseExam/year')
        .thenByDescending('courseExam/examPeriod')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  }
