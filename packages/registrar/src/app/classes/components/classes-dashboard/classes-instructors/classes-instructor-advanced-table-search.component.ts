import { Component, ViewEncapsulation } from '@angular/core';
import { AdvancedTableSearchBaseComponent } from 'packages/registrar/src/app/tables/components/advanced-table/advanced-table-search-base';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-classes-instructor-advanced-table-search',
  templateUrl: './classes-instructor-advanced-table-search.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ClassesInstructorAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

  constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
    super();
  }

}
