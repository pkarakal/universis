import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-classes-preview-form',
  templateUrl: './classes-form.component.html',
  styleUrls: ['./classes-form.component.scss']
})
export class ClassesFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _context: AngularDataContext) { }

  ngOnInit() {
  }

}
