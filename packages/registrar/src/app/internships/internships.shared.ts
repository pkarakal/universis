import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {InternshipsPreviewGeneralComponent} from './components/internships-preview-general/internships-preview-general.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {InternshipsPreviewFormComponent} from './components/internships-preview-general/internships-preview-form.component';
import {
  InternshipsDefaultTableConfigurationResolver,
  InternshipsTableConfigurationResolver,
  InternshipsTableSearchResolver
} from './components/internships-table/internships-table-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    InternshipsPreviewFormComponent
  ],
  providers: [
    InternshipsDefaultTableConfigurationResolver,
    InternshipsTableConfigurationResolver,
    InternshipsTableSearchResolver
  ],
  exports: [
    InternshipsPreviewFormComponent
  ]
})
export class InternshipsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading internships shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/internships.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
