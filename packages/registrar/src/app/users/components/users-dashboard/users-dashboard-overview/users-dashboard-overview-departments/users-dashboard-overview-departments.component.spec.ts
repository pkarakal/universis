import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDashboardOverviewDepartmentsComponent } from './users-dashboard-overview-departments.component';

describe('UsersDashboardOverviewDepartmentsComponent', () => {
  let component: UsersDashboardOverviewDepartmentsComponent;
  let fixture: ComponentFixture<UsersDashboardOverviewDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersDashboardOverviewDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDashboardOverviewDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
