import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalsDialogComponent } from './components/modals-dialog/modals-dialog.component';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {ModalModule} from 'ngx-bootstrap';
import { ModalsDoughnutComponent } from './components/modals-doughnut/modals-doughnut.component';
import {ChartsModule} from 'ng2-charts';


@NgModule({
  imports: [
    CommonModule,
    HighlightIncludeModule,
    ModalModule.forRoot(),
    ChartsModule
  ],
  declarations: [
    ModalsDialogComponent,
    ModalsDoughnutComponent
  ]
})
export class ModalsModule { }
