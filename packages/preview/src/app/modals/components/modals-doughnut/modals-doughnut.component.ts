import {Component, OnInit} from '@angular/core';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-modals-doughnut',
  templateUrl: './modals-doughnut.component.html',
  styleUrls: ['./modals-doughnut.component.scss']
})
export class ModalsDoughnutComponent implements OnInit {
  // Doughnut
  public doughnutChartLabels = ['Age 18 to 24', 'Age 25 to 35', 'Above 35+'];
  public demodoughnutChartData = [[350, 450, 100]];
  public doughnutChartType = 'doughnut';
  public doughnutOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    }
  };
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor() { }

  ngOnInit() {
  }

}
