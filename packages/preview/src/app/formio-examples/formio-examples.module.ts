import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormioModule } from 'angular-formio';
import { FormioExamplesBaseComponent } from './formio-examples-base/formio-examples-base.component';
import { FormioExamplesRoutingModule } from './formio-examples.routing';
import { HighlightIncludeModule } from '../highlight/highlight.module';

@NgModule({
  imports: [
    CommonModule,
    FormioModule,
    FormioExamplesRoutingModule,
    HighlightIncludeModule
  ],
  declarations: [FormioExamplesBaseComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FormioExamplesModule { }
