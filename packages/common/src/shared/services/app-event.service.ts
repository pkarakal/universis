import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class AppEventService {
    public change: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public add: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public remove: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    constructor() {
    }
}
