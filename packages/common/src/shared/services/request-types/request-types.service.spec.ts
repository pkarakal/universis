import { TestBed } from '@angular/core/testing';
import { RequestTypesService, RequestTypeItem } from './request-types.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AngularDataContext } from '@themost/angular';

describe('RequestTypesServiceService', () => {

  let service: RequestTypesService;

  const mockEntry1: RequestTypeItem = {
    name: 'mockEntry1',
    alternateName: 'mockEntry1',
    category: 'mockEntry1',
    entryPoint: 'mockEntry1',
    description: 'mockEntry1'
  };

  const mockEntry2: RequestTypeItem = {
    name: 'mockEntry2',
    alternateName: 'mockEntry2',
    category: 'mockEntry2',
    entryPoint: 'mockEntry2',
    description: 'mockEntry2'
  };

  const mockEntry3: RequestTypeItem = {
    name: 'mockEntry3',
    alternateName: 'mockEntry3',
    category: 'mockEntry3',
    entryPoint: 'mockEntry3',
    description: 'mockEntry3'
  };


  beforeEach((() => {
    return TestBed.configureTestingModule({
        declarations: [],
        imports: [
          HttpClientTestingModule,
        ],
        providers: [
          AngularDataContext
        ]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(RequestTypesService);
  });

  it('should be created', () => {
    service = TestBed.get(RequestTypesService);
    expect(service).toBeTruthy();
  });

  describe('While trying to add a new category', () => {
    describe('given a correct entry format', () => {
      it('should add an entry.', () => {
        service.add(mockEntry1);
        const list = service.getItems();
        expect(list.length).toEqual(1);
      });

      it('should add the specific entry entry.', () => {
        service.add(mockEntry1);
        const list = service.getItems();
        expect(list.length).toEqual(1);
        expect(list[0]).toEqual(mockEntry1);
      });

      it('should add many entries.', () => {
        service.add(mockEntry1);
        service.add(mockEntry2);
        service.add(mockEntry3);
        const list = service.getItems();
        expect(list.length).toEqual(3);
      });

      it('should not add duplicate items.',  () => {

        const duplicate = {
          ...mockEntry1
        };

        service.add(mockEntry1);
        service.add(duplicate);
        const list = service.getItems();
        expect(list.length).toEqual(1);
      });

      it('should not add duplicate items for intermediate items', () => {
        const duplicate = {
          ...mockEntry2
        };

        service.add(mockEntry1);
        service.add(mockEntry2);
        service.add(mockEntry3);
        service.add(duplicate);
        const list = service.getItems();
        expect(list.length).toEqual(3);
      });

      it('should add a range of items', () => {
        service.addRange(mockEntry1, mockEntry2, mockEntry3);
        const list = service.getItems();
        expect(list.length).toEqual(3);
      });

      it('should add handle duplicates at multiple adds', () => {

        const duplicate = {
          ...mockEntry2
        };

        service.add(mockEntry1);
        service.add(mockEntry2);
        service.add(mockEntry3);
        service.add(duplicate);
        const list = service.getItems();
        expect(list.length).toEqual(3);
      });
    });

    describe('given bad formatted object', () => {
      it('should throw exception for null item', () => {
        expect(() => service.add(null)).toThrowError();
      });

      it('should throw exception for undefined name', () => {
        const badMock = {
          ...mockEntry1,
          name: undefined
        };

        expect(() => service.add(badMock)).toThrowError();
      });

      it('should throw exception for undefined alternateName', () => {
        const badMock = {
          ...mockEntry1,
          alternateName: undefined
        };

        expect(() => service.add(badMock)).toThrowError();
      });

      it('should throw exception for undefined category', () => {
        const badMock = {
          ...mockEntry1,
          category: undefined
        };

        expect(() => service.add(badMock)).toThrowError();
      });
    });

    it('should add item by validating case-insensitive alternateName', () => {
      service.add({
        alternateName: 'requestType',
        category: 'category',
        name: 'name',
        description: 'description',
        entryPoint: '#/path/to/entry/point'
      });

      service.add({
        alternateName: 'RequestType',
        category: 'category',
        name: 'name',
        description: 'description',
        entryPoint: '#/path/to/entry/point'
      });

      expect(service.getItems().length).toBe(1);
    });
  });

  describe('while trying to remove an item', () => {

    beforeEach(() => {
      service.addRange(mockEntry1, mockEntry2, mockEntry3);
    });

    it('should remove an item', () => {
      service.remove(mockEntry1);
      const list = service.getItems();
      expect(list.length).toEqual(2);
    });

    it('should remove a specific item', () => {
      const removed = service.remove(mockEntry1);
      const list = service.getItems();

      expect(list[0]).not.toEqual(mockEntry1);
      expect(list[1]).not.toEqual(mockEntry1);
      expect(removed).toEqual(mockEntry1);
    });


    it('should remove items by their alternate name', () => {
      const removed = service.removeByName(mockEntry1.alternateName);
      const list = service.getItems();

      expect(list[0]).not.toEqual(mockEntry1);
      expect(list[1]).not.toEqual(mockEntry1);
      expect(removed).toEqual(mockEntry1);
    });

    it('should throw error on null item', () => {
      expect(() => service.remove(null)).toThrowError();
    });

    it('should not change the list for not found items', () => {

      const notFound = {
        ...mockEntry2,
        alternateName: 'I should not be found'
      };

      service.remove(notFound);
      const list = service.getItems();
      expect(list.length).toEqual(3);
    });

    it('should return null if the item not found', () => {

      const notFound = {
        ...mockEntry2,
        alternateName: 'I should not be found'
      };

      const deleted = service.remove(notFound);
      expect(deleted).toBeFalsy();
    });

    it('should remove item by validating case-insensitive alternateName', () => {
      const caseSensitiveMockAltered = {
        ...mockEntry1,
        alternateName: mockEntry1.alternateName.toUpperCase()
      };

      service.remove(caseSensitiveMockAltered);
      expect(service.getItems().length).toBe(2);
    });
  });
});
