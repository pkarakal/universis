import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ErrorModule} from './error.module';
import {ErrorService} from './error.service';
import {RouterModule} from '@angular/router';
import {ErrorsHandler} from './error.handler'
import {APP_BASE_HREF} from '@angular/common';
import {ModalModule, BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

describe('ErrorModule', () => {
  const _modalSpyObj = jasmine.createSpyObj('BsModalService', ['show']);
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
          RouterModule.forRoot([]),
          TranslateModule.forRoot(),
          ErrorModule.forRoot(),
          ModalModule.forRoot()
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        BsModalRef,
        ErrorService,
        ErrorsHandler,
        {
          provide: BsModalService,
          useValue: _modalSpyObj
        }
      ]
    }).compileComponents();
  }));
  it('should inject service', inject([ErrorService, TranslateService],
      (errorService: ErrorService, translateService: TranslateService,) => {
        expect(errorService).toBeTruthy();
        translateService.setDefaultLang('en');
        expect(translateService.instant('Error.Continue')).toBe('Continue');
  }));
    it('should get translation added by error module', inject([ErrorService, TranslateService],
        (errorService: ErrorService, translateService: TranslateService) => {
            expect(errorService).toBeTruthy();
            translateService.use('el');
            expect(translateService.instant('Error.Continue')).toBe('Συνέχεια');
        }));
});
