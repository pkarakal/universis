import {Injectable} from '@angular/core';
import {RequestMatch, TestRequest} from '@angular/common/http/testing';
import {HttpRequest} from '@angular/common/http';


@Injectable()
export class ApiTestingController {
    private _match: RequestMatch;
    private readonly matches: Map<RequestMatch, (request: TestRequest) => void>;
    constructor() {
        // initialize map
        this.matches = new Map<RequestMatch, (request: TestRequest) => void>();
    }

    /**
     * Prepares a request mapper based on the given request match
     * @param match
     */
    match(match: RequestMatch): ApiTestingController {
        // set temp request and prepare to get request mapper
        this._match = match;
        return this;
    }

    /**
     * Maps a request (based on an in-process request match)
     * with an instance of TestRequest class
     * @param mapper
     */
    map(mapper: (request: TestRequest) => void): ApiTestingController {
        if (this._match) {
            // search if match.url already exists
            const keys = Array.from(this.matches.keys());
            // find key by method and url
            const key = keys.find(value => {
                return value.method === this._match.method && value.url === this._match.url;
            });
            if (key) {
                // update existing item
                this.matches.set(key, mapper);
            } else {
                // add new
                this.matches.set(this._match, mapper);
            }
            this._match = null;
            return this;
        }
        throw new TypeError('Request match cannot be empty at this context. ' +
            'Call CommonTestHttpController.match() first.');
    }

    /**
     * Finds a request mapper based on the given request matcj
     * @param req
     */
    find(req: HttpRequest<any>): (request: TestRequest) => void {
        // get keys
        const keys = Array.from(this.matches.keys());
        // find key by method and url
        const key = keys.find(value => {
            return value.method === req.method && value.url === req.url;
        });
        // and return request mapper if any
        if (key) {
            return this.matches.get(key);
        }
    }
}
